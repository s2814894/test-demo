package net.contal.demo.services;

import net.contal.demo.DbUtils;
import net.contal.demo.modal.CustomerAccount;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Transactional
public class BankServiceTest {

    public static DbUtils dbUtils;


    @BeforeClass
    public static void init()
    {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("net.contal.demo");
        context.refresh();
        dbUtils = (DbUtils) context.getBean("dbUtils");
    }

    @Test
    public void itCreatesANewAccountNumber()
    {
        CustomerAccount customerAccount = getSampleCustomerAccount();

        new BankService(dbUtils).createAnAccount(customerAccount);

        CustomerAccount result = dbUtils.openASession().get(CustomerAccount.class, customerAccount.getId());

        assertEquals(result.getFirstName(), customerAccount.getFirstName());
        assertEquals(result.getLastName(), customerAccount.getLastName());
        assertEquals(result.getAccountNumber(), customerAccount.getAccountNumber());
        assertEquals(result.getAccountBalance(), customerAccount.getAccountBalance());
    }

    @Test
    public void itUpdatesTheAccountBalanceAfterTransactionIsCreated()
    {
        CustomerAccount customerAccount = getSampleCustomerAccount();
        BankService bankService = new BankService(dbUtils);
        bankService.createAnAccount(customerAccount);

        bankService.addTransactions(customerAccount.getAccountNumber(), -10.0);

        CustomerAccount result = dbUtils.openASession().get(CustomerAccount.class, customerAccount.getId());

        assertEquals(customerAccount.getAccountBalance() - 10, result.getAccountBalance());
    }

    @Test
    public void itCreatesABankTransactionForTheAccount()
    {
        CustomerAccount customerAccount = getSampleCustomerAccount();
        BankService bankService = new BankService(dbUtils);
        bankService.createAnAccount(customerAccount);

        bankService.addTransactions(customerAccount.getAccountNumber(), -10.0);

        CustomerAccount result = dbUtils.openASession().get(CustomerAccount.class, customerAccount.getId());


        assertEquals(-10, result.getBankTransactions().get(0).getTransactionAmount());
    }

    @Test
    public void itReturnsFalseIfTheAccountNumberDoesNotExist()
    {
        assertFalse(new BankService(dbUtils).addTransactions(123, -10.0));
    }

    @Test
    public void itReturnsAccountNumberWhenItCreatesAnAccount()
    {
        CustomerAccount customerAccount = new CustomerAccount();

        String accountNumber = new BankService(dbUtils).createAnAccount(customerAccount);

        assertEquals(String.valueOf(customerAccount.getAccountNumber()), accountNumber);
    }

    @Test
    public void itCalculatesTheAccountBalance()
    {
        CustomerAccount customerAccount = getSampleCustomerAccount();
        BankService bankService = new BankService(dbUtils);
        bankService.createAnAccount(customerAccount);

        bankService.addTransactions(customerAccount.getAccountNumber(), -100.0);
        bankService.addTransactions(customerAccount.getAccountNumber(), -100.0);
        bankService.addTransactions(customerAccount.getAccountNumber(), -100.0);


        assertEquals(-300.0, bankService.getBalance(customerAccount.getAccountNumber()));
    }

    private CustomerAccount getSampleCustomerAccount() {
        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setFirstName("John");
        customerAccount.setLastName("Smith");
        customerAccount.setAccountNumber(12);
        customerAccount.setAccountBalance(1000.0);
        return customerAccount;
    }

}
