package net.contal.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountNumberUtilTests {

	@Test
	void returnsARandomNumber() {
		assertTrue(AccountNumberUtil.generateAccountNumber() != AccountNumberUtil.generateAccountNumber());
	}

}
