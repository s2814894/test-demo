package net.contal.demo.model;

import net.contal.demo.modal.BankTransaction;
import org.junit.jupiter.api.Test;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BankTransactionTest {

	@Test
	void itAllowsToSetAndGetTransactionAmount() {
		BankTransaction bankTransaction = new BankTransaction();

		bankTransaction.setTransactionAmount(100.00);

		assertEquals(100.00, bankTransaction.getTransactionAmount());
	}

	@Test
	void itAllowsToSetAndGetTransactionDate() {
		BankTransaction bankTransaction = new BankTransaction();
		Date date = new Date();

		bankTransaction.setTransactionDate(date);

		assertEquals(date, bankTransaction.getTransactionDate());
	}

}
