package net.contal.demo.model;

import net.contal.demo.modal.BankTransaction;
import net.contal.demo.modal.CustomerAccount;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomerTransactionTest {

	@Test
	void itAllowsToSetAndGetFirstName() {
		CustomerAccount customerAccount = new CustomerAccount();

		customerAccount.setFirstName("Jamie");

		assertEquals("Jamie", customerAccount.getFirstName());
	}

	@Test
	void itAllowsToSetAndGetLastName() {
		CustomerAccount customerAccount = new CustomerAccount();

		customerAccount.setLastName("Smith");

		assertEquals("Smith", customerAccount.getLastName());
	}

	@Test
	void itAllowsToSetAndGetAccountNumber() {
		CustomerAccount customerAccount = new CustomerAccount();

		customerAccount.setAccountNumber(12341234);

		assertEquals(12341234, customerAccount.getAccountNumber());
	}

	@Test
	void itAllowsToSetAndGetAccountBalance() {
		CustomerAccount customerAccount = new CustomerAccount();

		customerAccount.setAccountBalance(100.00);

		assertEquals(100.00, customerAccount.getAccountBalance());
	}
}
