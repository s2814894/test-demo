package net.contal.demo;

import net.contal.demo.modal.CustomerAccount;
import net.contal.demo.services.BankService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DemoApplicationTests {
	@Autowired
	public DbUtils dbUtils;
	@Test
	void contextLoads() {
		CustomerAccount customerAccount = new CustomerAccount();

		String accountNumber = new BankService(dbUtils).createAnAccount(customerAccount);

		assertEquals(String.valueOf(customerAccount.getAccountNumber()), accountNumber);
	}

}
