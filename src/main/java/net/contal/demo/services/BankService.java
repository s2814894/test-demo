package net.contal.demo.services;

import net.contal.demo.AccountNumberUtil;
import net.contal.demo.DbUtils;
import net.contal.demo.modal.BankTransaction;
import net.contal.demo.modal.CustomerAccount;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.Tuple;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional
public class BankService {

    //USE this class to access database , you can call openASession to access database
    private final DbUtils dbUtils;
    @Autowired
    public BankService(DbUtils dbUtils) {
        this.dbUtils = dbUtils;
    }

    public String createAnAccount(CustomerAccount customerAccount){
        customerAccount.setAccountNumber(AccountNumberUtil.generateAccountNumber());
        Session session = dbUtils.getHibernateFactory().openSession();
        session.beginTransaction();
        session.save(customerAccount);
        session.getTransaction().commit();
        session.close();
        return String.valueOf(customerAccount.getAccountNumber());
    }


    public boolean addTransactions(int accountNumber , Double amount)
    {
        String hql = "FROM CustomerAccount WHERE accountNumber = :accountNumber";

        Session session = dbUtils.getHibernateFactory().openSession();
        session.beginTransaction();
        List<CustomerAccount> resultList = session.createQuery(hql, CustomerAccount.class)
                .setParameter("accountNumber", accountNumber)
                .getResultList();
        session.getTransaction().commit();

        if (resultList.isEmpty() || BigDecimal.valueOf(amount).compareTo(BigDecimal.valueOf(0.0)) == 0) {
            session.close();
            return  false;
        }

        CustomerAccount customerAccount = resultList.get(0);
        customerAccount.setAccountBalance(customerAccount.getAccountBalance() + amount);

        BankTransaction bankTransaction = new BankTransaction(amount, new Date());

        customerAccount.getBankTransactions().add(bankTransaction);
        bankTransaction.setCustomerAccount(customerAccount);

        session.beginTransaction();
        session.save(bankTransaction);
        session.update(customerAccount);
        session.getTransaction().commit();
        session.close();


        return true;
    }

    public double getBalance(int accountNumber)
    {
        Session session = dbUtils.getHibernateFactory().openSession();
        session.beginTransaction();
        String hql = "SELECT SUM(t.transactionAmount) FROM BankTransaction t " +
                "INNER JOIN t.customerAccount c " +
                "where c.accountNumber = :accountNumber ";
        Double sumAmount = (Double) session.createQuery(hql)
                .setParameter("accountNumber", accountNumber)
                .getSingleResult();

        session.getTransaction().commit();
        session.close();

        return sumAmount;
    }

    public Map<Date,Double> getDateBalance(int accountNumber){
        Session session = dbUtils.getHibernateFactory().openSession();
        session.beginTransaction();
        String hql = "SELECT t.transactionDate as date, t.transactionAmount as amount FROM BankTransaction t " +
                "INNER JOIN t.customerAccount c " +
                "where c.accountNumber = :accountNumber ";
        Map<Date, Double> map = session.createQuery(hql, Tuple.class)
                .setParameter("accountNumber", accountNumber)
                .getResultStream()
                .collect(Collectors.toMap(tuple ->( (Date) tuple.get("date")),
                        tuple -> ((Double) tuple.get("amount"))));

        session.getTransaction().commit();
        session.close();

        return map;
    }
}
